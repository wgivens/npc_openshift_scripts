#!/bin/bash

oc whoami > /dev/null 2>&1
LOGGEDIN=$?
if [ "$LOGGEDIN" != 0 ]; then
    echo "ERROR:  Please login to OpenShift prior to executing this script."
    exit 0
fi

watch --differences=cummulative -n4 'oc get nodes'