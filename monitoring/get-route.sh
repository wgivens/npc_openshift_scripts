#!/bin/bash

if [[ ! $1 ]];
then
  echo "Please enter a route to search for, usage - ./getroute.sh <search param>"
  exit 0
fi

oc whoami > /dev/null 2>&1
LOGGEDIN=$?
if [ "$LOGGEDIN" != 0 ]; 
then
    echo "ERROR:  Please login to OpenShift prior to executing this script."
    exit 0
else
    oc get route --all-namespaces | grep $1
fi

