#!/bin/bash
# Script to transfer deployment config replicas
# Run from the bastion host of the new target cluster

#base variables
project_error=()

# Old cluster credentials (mci1ocp)
user_token="Hfi045MLTzqZLWvOnP3SgRlp2LcGaDKnvR4XKgBJvkQ"
token="Authorization: Bearer $user_token"
base_url="https://api.mci1ocp.netsmartcore.lan:6443/apis"

# New cluster credentials (mci1cp01)
new_cluser_token="Authorization: Bearer $(oc whoami -t)"
new_cluser_base_url="https://api.mci1cp01.netsmartcore.lan:6443/apis"

# Verify oc login to new cluster
oc whoami > /dev/null 2>&1
LOGGEDIN=$?
if [ "$LOGGEDIN" != 0 ]; then
    echo "ERROR:  Please login to OpenShift prior to executing this script."
    exit 0
fi

# Verify api authentication to old cluster
auth_check=$(curl -sk -H "$token" "$base_url/oauth.openshift.io/v1/oauthclientauthorizations" | jq .status |sed 's/"//g')
if [ "$auth_check" == "Failure" ]; then
    echo "ERROR:  Unable to authenticate with old cluster.  Please use a valid authorization token in the script (user_token)."
    exit 0
fi

# Get existing Projects on the old cluster
echo -e "Gathering Projects...\n"
# Match projects with client ID number
projects=$(curl -sk -H "$token" "$base_url/project.openshift.io/v1/projects" | jq '.items[].metadata.name | match("^(?!21212|99999)([0-9]{5})-.*?$"; "g")| .string'|sed 's/"//g')

# Loop through all found projects
for project in ${projects[@]}; do 
    # Check if the project exists on the new cluster
    project_exists=$(curl -s -k -H "$new_cluser_token" "$new_cluser_base_url/project.openshift.io/v1/projects/$project" | jq .metadata.name |sed 's/"//g')

    if [[ $project_exists != 'null' ]]; then
        # Switch to project on the new cluster
        oc_project=$(oc project $project)
        echo -e $oc_project
        echo -e "Selected Project | $project\n-- Deployment Configs --"

        # Get all deployment configs for the current project
        project_dc=$(curl -s -k -H "$token" "$base_url/apps.openshift.io/v1/namespaces/$project/deploymentconfigs/" | jq .items[].metadata.name |sed 's/"//g')

        # Loop through deployment configs for the current project
        for dc in ${project_dc[@]}; do
            # Get the replica value for the current deployment config
            current_dc_replicas=$(curl -s -k -H "$token" "$base_url/apps.openshift.io/v1/namespaces/$project/deploymentconfigs/$dc/scale/" | jq .status.replicas)
            echo -e "$dc | Replicas: $current_dc_replicas"

            # Scale the deployment config in the new cluster
            # Remove '--dry-run=client' to actually run command
            oc_scale=$(oc scale --replicas=$current_dc_replicas dc/$dc)
            echo $oc_scale

        done
    else # If the project is not found on the new cluster
        echo -e "\nError: $project not found - Skipping"
        project_error+=($project)
    fi
    
done

# List projects with errors
echo -e "\nProjects not scaled:"
for error in "${project_error[@]}"; do
    echo $error
done
