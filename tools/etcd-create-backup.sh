#!/bin/bash

# Check if user id logged into openshift console
oc whoami > /dev/null 2>&1
LOGGEDIN=$?
if [ "$LOGGEDIN" != 0 ]; then
    echo "ERROR:  Please login to OpenShift prior to executing this script."
    exit 0
fi

# Get master node to create debug pod
oc project default
MASTER=$(oc get nodes -l node-role.kubernetes.io/master -o wide | awk 'NR == 2 {print $1}')
echo "Selected Master Node: $MASTER"

# Create debug pod on a master node, then create etc backup via cluster-backup.sh
backup_cluster() {
cat <<EOF | oc debug node/$MASTER
chroot /host
mkdir /tmp/etcd_$(date +"%Y-%m-%d") && /usr/local/bin/cluster-backup.sh /tmp/etcd_$(date +"%Y-%m-%d")
sleep 20
rm -rf /tmp/etcd_$(date +"%Y-%m-%d")
exit
exit
EOF
}

# Copy backup from cluster debug pod to local directory
backup_local() {
    sleep 15    # Wait for backup to be created on the cluster
    DEBUGPROJECT=$(oc describe node $MASTER | grep debug-node | awk 'NR == 1 {print $1}')
    oc project $DEBUGPROJECT
    DEBUGPOD=$(oc get pods | grep debug | awk '{print $1}')
    oc rsync $DEBUGPOD:/host/tmp/etcd_$(date +"%Y-%m-%d") .  # Copy backup on pod to local 
    oc project default
}

# Spawn coprocess for backup on cluster
coproc backup_cluster

# Concurrently run local backup function
backup_local

# Wait for functions to finish
wait
